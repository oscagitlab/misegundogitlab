const express = require('express');
const router = express.Router();

const mysqlConnection  = require('../database.js');

// GET all Employees
router.get('/', (req, res) => {
  mysqlConnection.query('SELECT * FROM students', (err, rows, fields) => {
    if(!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });  
});

// Obtiene un Usuario
router.get('/:id', (req, res) => {
  const { id } = req.params; 
  mysqlConnection.query('SELECT * FROM students WHERE id = ?', [id], (err, rows, fields) => {
    if (!err) {
      res.json(rows[0]);
    } else {
      console.log(err);
    }
  });
});

// DELETE un Usuario
router.delete('/:id', (req, res) => {
  const { id } = req.params;
  mysqlConnection.query('DELETE FROM students WHERE id = ?', [id], (err, rows, fields) => {
    if(!err) {
      res.json({status: 'Estudiante Borrado'});
    } else {
      console.log(err);
    }
  }); 
});

// INSERT un Usuario
router.post('/', (req, res) => {
  const {id, name, address, phone, photo} = req.body;
  console.log(id, name, address, phone, photo);
  const query = `
    
    CALL studentsAddOrEdit(?,?,?,?,?);
  `;
  mysqlConnection.query(query, [id, name, address, phone, photo], (err, rows, fields) => {
    if(!err) {
      res.json({status: 'Empleado Salvado'});
    } else {
      console.log(err);
    }
  });

});

router.put('/:id', (req, res) => {
  const { name, adress, phone, photo } = req.body;
  const { id } = req.params;
  const query = `
   
    CALL studentsAddOrEdit(?,?,?,?,?);
  `;
  mysqlConnection.query(query, [id, name, address, phone, photo], (err, rows, fields) => {
    if(!err) {
      res.json({status: 'Estudiante Actualizado'});
    } else {
      console.log(err);
    }
  });
});

module.exports = router;
