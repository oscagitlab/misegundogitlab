var mysql = require('mysql');
function Conexion(){
    this.pool = null;

    this.inicia = function(){
    this.pool = mysql.createPool({
    connectionLimit: 1020,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'sob'
    
    });
    }
    this.obtener = function(callback) {
        this.pool.getConnection(function(error, connection){
            callback(error, connection);
        })
    }
}
module.exports= new Conexion();
