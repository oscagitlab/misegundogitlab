var express = require('express');
var bodyparser = require('body-parser');
var jwt = require('jsonwebtoken');
//instancia de express
var app = express();
var SECRET_KEY = "Marmota";
let mysql  = require('mysql');
const  router  =  express.Router();

var cors = require('cors');
app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var connection = require('./connection');
var routes = require('./routes');
connection.inicia();

// Implementando token
app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json({limit:'10mb'}))
app.use(express.json());

routes.configurar(app);
var server = app.listen(5034, function(){
    console.log('escuchando en el puerto 5034',server.address().port);

})