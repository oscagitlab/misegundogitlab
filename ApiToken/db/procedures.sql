USE sob;

DELIMITER $$
USE `sob`$$

BEGIN 
  IF _id = 0 THEN
    INSERT INTO students (name, address, phone, photo)
    VALUES (_name, _address, _phone, _photo);

    SET _id = LAST_INSERT_ID();
  ELSE
    UPDATE students
    SET
    name = _name,
    address = _address,
    phone = _phone,
    photo = _photo
    WHERE id = _id;
  END IF;

  SELECT _id AS id;
END